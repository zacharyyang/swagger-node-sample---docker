# Get started your swagger mock API 


#### Pull image from docker hub ( https://hub.docker.com/r/zacharyyang/sg-node ) :
```
$ docker pull zacharyyang/sg-node
```

#### Run image

```
$ docker run -it --net=host -v </the-project-in-host>:/doc --name <container-name> zacharyyang/sg-node
```
The volume between host (*"<the-project-in-host>"*) and container(*"/doc"*), letting you easier to edit your project in host. 
[see more..](https://docs.docker.com/storage/volumes/)
#### Create a new swagger project

```
$ swagger project create <your-porject-name>
```
#### Pick the API framework you want to use. We're going to pick express, but you can pick any of the listed frameworks:
```
? Framework? (Use arrow keys)
  connect
❯ express
  hapi
  restify
  sails
```
#### Go to the porject and start your mock api server
```
$ cd <your-porject-name>
$ swagger project start <your-porject-name> -m
```

![](https://i.imgur.com/iYpGf29.png)

http://<ip-address:10010>/<restful-url>

![](https://i.imgur.com/1RCKoRJ.png)

## Open another command line

```
$ docker exec -ti <container-name> /bin/sh
```


#### Go to the porject dir and start your swagger edit
```
$ cd <your-porject-name>
$ swagger project edit --host <ip-address> -s
```

![](https://i.imgur.com/HSBnrtS.png)

http://<ip-address:port>/#!/
![](https://i.imgur.com/Ghh3FU4.png)

## Paste your swagger document *yaml/json* in swagger edit

#### Need to define three places as follows:

Add the address and port in top :
- host: the address of the mock api server.

Short pieces of yaml is what we need to define our routes in each paths, then the http methods must be listed:

- x-swagger-router-controller: This is the controller, the file from in the *your-project-name/api/mocks/<operationId>.js*.
- operationId: This refers to the function, in the controller, in charge of the business logic.

![](https://i.imgur.com/HTVv9wP.png)


Otherwise, if not satisfied by the standard values, it is always possible to define the mock controller in *<your-project-name>/api/mocks/<x-swagger-router-controller_name>.js* and add a operationId. Then, the business logic just returns the answer in the format you defined in the definitions, but you can customize the values for each property. 

![](https://i.imgur.com/jmjr4yu.png)

After add your own example, you need to change the **validateResponse** to **false**, the file is in 
*<your-project-name>/api/config/default.js* 


![](https://i.imgur.com/iiG7t9P.png)

done!!

----------------------------------------------
## Reference
1. swagger-api/swagger-node: https://github.com/swagger-api/swagger-node
2. swagger-node/docs/quick-start.md: https://github.com/swagger-api/swagger-node/blob/master/docs/quick-start.md
3. Speed up your RESTful API development in Node.js with Swagger: 
https://scotch.io/tutorials/speed-up-your-restful-api-development-in-node-js-with-swagger
4. Swagger+Mock API+Vue： 
https://docs.google.com/presentation/d/1hzgbRKARxBGOJiTwfoDHbgWOmDRNSBW2fdimAGuXfCU/edit?usp=sharing

